# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 16:16:01 2016

@author: etudiant
"""

from setuptools import setup

setup(
	name="dcplaylist",
	version="0.1",
	author="Clément CAUDAL",
	author_email="clement.caudal@bts-malraux.net",
	description="Automatic musics playlist generation program (PPE3)",
	long_description="This is the programm developpe in python to generate a playlist during own personnal project",
	license="GPLv3",
	url="https://framagit.org/workgroupDC/generateur-de-playlist.git",
	packages=['ppe3_generateur_de_playlist', 'ppe3_generateur_de_playlist.ressources'],
	data_files=[('/usr/share/man/man1', ['doc/dcplaylist.1']), ('/usr/share/doc/python3-dcplaylist', ['doc/dcplaylist.pdf','doc/dcplaylist.html'])],
	entry_points={
		'console_scripts': [
			'dcplaylist = ppe3_generateur_de_playlist.generatorPlaylist:main'
		]
	}
)
