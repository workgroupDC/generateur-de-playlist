# -*- coding: utf-8 -*-

"""
Created on Tue Sep 27 16:27:21 2016

@author: Clément CAUDAL
"""

import logging
from logging.handlers import RotatingFileHandler
from .genre import Genre
from .artist import Artist
from .album import Album
from .track import Track
from .format import Format
from .polyphony import Polyphony
import postgresql

logger = logging.getLogger("playlist")
logger.setLevel(logging.DEBUG)

""" Write in a syslog 'dclog.log'
"""
formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(filename)s:%(message)s')
file_handler = RotatingFileHandler('dclog.log','a',1000000,1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


class Playlist(object):
    def __init__(self, name, duration, listgenre, listartist, listalbum, listtrack):
        """Constructor of class Playlist
        """
        logger.debug("Instanciation d'une 'playlist'")
        self.name = str(name)
        self.duration = int(duration)
        self.listgenre = listgenre
        self.listartist = listartist # List artist.Artist()
        self.listalbum = listalbum # List album.Album()
        self.listtrack = listtrack # List track.Track()

    #----GETTERS----#
    def getName(self):
        """Return name playlist as a str()
        """
        return self.name

    def getDuration(self):
        """Return deuration playlist as a int()
        """
        return self.duration

    def getGenre(self):
        """Return genre playlist as a str()
        """
        return self.listgenre

    def getArtistlist(self):
        """Return artist list playlist as a List(Artist)
        """
        return self.listartist
    
    def getAlbumlist(self):
        """Return album list playlist as a List(Album)
        """
        return self.listalbum

    def getElementPERL(self):
        """Return elementPERL playlist as a str()
        """
        pass
    
    def getListtrack(self):
        """Return track list playlist as a list(Track)
        """
        return self.listtrack


    #----SETTERS----#
    def setName(self, _name):
        """Set playlist name using the given parameter str()
        """
        old_name = self.name # new variable old_name which takes the value of the former name of the playlist
        self.name = str(_name) # modification of the playlist name
        logger.debug("Playlist name is now '" + self.name + "' (was '" + old_name + "')") # message of modification playlist name


    def setDuration(self, _duration):
        """Set playlist duration using the given parameter int()
        """
        old_duration = self.duration # same thing as for the playlist name
        self.duration = int(_duration)
        logger.debug("Playlist duration is now '" + self.duration + "' (was '" + old_duration + "')'") # modification playlist duration


    def setListgenre(self, _listgenre):
        """Set playlist list genre using the given parameter Genre()
        """
        old_genre = self.listgenre
        self.listgenre = _listgenre
        logger.debug("Playlist genre list is now modified") # modification playlist list genre


    def setListartist(self, _listartist):
        """Set playlist artist list using the given parameter Artist()
        """
        old_listartist = self.listartist
        self.listartist = _listartist
        logger.debug("Playlist artist list is now modified") # modification playlist list artist


    def setListalbum(self, _listalbum):
        """Set playlist album list using the given parameter Album()
        """
        old_listalbum = self.listalbum
        self.listalbum = _listalbum
        logger.debug("Playlist album list is now modified") # modification playlist list album


    def setElementPERL(self, _elementPERL):
        """Set playlist elementPERL using the given parameter str()
        """
        old_elementPERL = self.elementPERL
        self.elementPERL = str(_elementPERL)
        logger.debug("Playlist genre is now '" + self.elementPERL + "' (was '" + old_elementPERL + "')'") # modification playlist elementPERL

    def setListtrack(self, _listtrack):
        """Set playlist track list using the given parameter Track()
        """
        old_listtrack = self.listtrack
        self.listtrack = _listtrack
        logger.debug("Playlist track list is now modified") # modification playlist list track


    def addTrack(self, _numTrack):
        """Add a track to a playlist using the given parameter int(numTrack)
        """
        db = postgresql.open('pq://clement_donovan_user:dcplaylist@172.16.99.2:5432/radio_libre')
        ps = db.prepare("SELECT * FROM clement_donovan.track INNER JOIN clement_donovan.artist ON track.id_artist = artist.id_artist INNER JOIN clement_donovan.album ON track.id_album = album.id_album INNER JOIN clement_donovan.format ON track.id_format = format.id_format INNER JOIN clement_donovan.genre ON track.id_genre = genre.id_genre INNER JOIN clement_donovan.polyphony ON track.id_polyphony = polyphony.id_polyphony WHERE id_track = $1")
        var = ps(_numTrack)
        track = var[0]
        title = track[1]
        path = track[2]
        duration = track[3]
        
        nom_album = track[12]
        alb = Album(nom_album)        
        
        nom_artist = track[10]   
        art = Artist(nom_artist)        
        
        nom_genre = track[16]
        gen = Genre(nom_genre)
        
        nom_format = track[14]
        form = Format(nom_format)
        
        nom_polyphony = track[18]
        pol = Polyphony(nom_polyphony)
        
        t = Track(title,duration,path,alb,art,gen,form,pol)
        self.listtrack.append(t)
        db.close()
        logger.debug(t.getTitle() + " has been added to tracklist !")

        
    def deleteTrack(self, _titleTrack):
        """delete a track to a playlist using the given parameter str(_titleTrack)
        """
        for t in self.listtrack:
            if t.getTitle() == _titleTrack:
                self.listtrack.remove(t)
        logger.debug(t.getTitle() + " has been removed to tracklist !")
        
    def exportPlaylist(self, _type):
        """ Export playlist using the type given parameter str(_type)
        """
        if _type == "m3u":
            fichier = open(self.getName()+".m3u", "w")
            logger.debug(self.listtrack)
            for t in self.listtrack:
                fichier.write(t.getPath()+"\n")
            fichier.close()
            logger.debug(self.getName() + " has been exported to ."+_type+" !")
        else:
            if _type == "xspf":
                fichier = open(self.getName()+".xspf", "w")
                fichier.write("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
                fichier.write("<playlist version =\"1\">\n")
                fichier.write("\t<trackList>\n")
                for t in self.listtrack:
                    fichier.write("\t\t<track>")
                    fichier.write("<location>"+t.getPath()+"</location>")
                    fichier.write("</track>\n")
                fichier.write("\t</trackList>\n")
                fichier.write("</playlist>")
                fichier.close()
                logger.debug(self.getName() + " has been exported to ."+_type+" !")

if __name__ == "__main__":
    logger.debug("Execution of module 'playlist.py'")
    gg = []
    aa = []
    aarr = []
    tt = []
    ff = []
    pp = []
    p = Playlist("abcd", 250, gg, aarr, aa, tt)
    p.addTrack(2)
    p.addTrack(1)
    p.addTrack(3)
    logger.debug(p.listtrack[0].getTitle())
    logger.debug(p.listtrack[1].getTitle())
    logger.debug(p.listtrack[2].getTitle())
    p.exportPlaylist("xspf")
    
    p.deleteTrack("trackUn")
    logger.debug("At the end of execution of the module 'playlist.py")
else:
    logger.debug("Loading of module 'playlist.py'")

