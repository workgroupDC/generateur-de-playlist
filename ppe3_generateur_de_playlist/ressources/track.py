# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 18:36:25 2016

@author: Donovan

"""

import logging
from logging.handlers import RotatingFileHandler
from .genre import Genre
from .artist import Artist
from .album import Album
from .format import Format
from .polyphony import Polyphony

logger = logging.getLogger("track")
logger.setLevel(logging.DEBUG)

""" Write in a syslog 'dclog.log'
"""
formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(filename)s:%(message)s')
file_handler = RotatingFileHandler('dclog.log','a',1000000,1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

class Track(object):
    def __init__(self, title, duration, path, album, artist, genre,format,polyphony):
        """Track constructor
        """
        logger.debug("Instantiation of a 'Track'")
        self.title = str(title)
        self.duration = int(duration)
        self.path = str(path)
        self.genre = genre
        self.artist = artist
        self.album = album
        self.format = format
        self.polyphony = polyphony

    def getTitle(self):
        """Return track title as a str()
        """
        return self.title
    
    def setTitle(self, _title):
        """Set track title using the given parameter str()
        """
        old_title = self.title
        self.title = str(_title)
        logger.debug("Track title is now: '" + self.title + "' (was '" + old_title + "'.)")
    
    def getDuration(self):
        """Return track duration as a int()
        """
        return self.duration
    
    def setDuration(self, _duration):
        """Set track duration using the given parameter int()
        """
        old_duration = self.duration
        self.duration = str(_duration)
        logger.debug("Track duration is now: '" + str(self.duration) + "s' (was '" + str(old_duration) + "s'.)")
        
    def getPath(self):
        """Return track path as a str()
        """
        return self.path
    
    def setPath(self, _path):
        """Set track path using the given parameter str()
        """
        old_path = self.path
        self.path = str(_path)
        logger.debug("Track path is now: '" + self.path + "' (was '" + old_path + "'.)")
    
    def getGenre(self):
        """Return track genre as a genre.Genre()
        """
        return self.genre
    
    def setGenre(self, _genre):
        """Set track genre using the given parameter genre.Genre()
        """
        old_genre = self.genre.getWording()
        self.genre = _genre
        logger.debug("Track genre is now: '" + self.genre.getWording() + "' (was '" + old_genre + "'.)")
    
    def getArtist(self):
        """Return track artist as a artist.Artist()
        """
        return self.artist
    
    def setArtist(self, _artist):
        """Set track artist using the given parameter artist.Artist()
        """
        old_artist = self.artist.getStagename()
        self.artist = _artist
        logger.debug("Track artist is now: '" + self.artist.getStagename() + "' (was '" + old_artist + "'.)")
    
    def getAlbum(self):
        """Return track album as a album.Album()
        """
        return self.album
    
    def setAlbum(self, _album):
        """Set track album using the given parameter album.Album()
        """
        old_album = self.album.getTitle()
        self.album = _album
        logger.debug("Track album is now: '" + self.album.getTitle() + "' (was '" + old_album + "'.)")
        
    def getFormat(self):
        """Return track format as a format.Format()
        """
        return self.format
    
    def setFormat(self, _format):
        """Set track format using the given parameter format.Format()
        """
        old_format = self.format.getWording()
        self.format = _format
        logger.debug("Track format is now: '" + self.format.getWording() + "' (was '" + old_format + "'.)")
    
    def getPolyphony(self):
        """Return track polyphony as a polyphony.Polyphony()
        """
        return self.polyphony
    
    def setPolyphony(self, _polyphony):
        """Set track polyphony using the given parameter polyphony.Polyphony()
        """
        old_polyphony = self.polyphony.getWording()
        self.polyphony = _polyphony
        logger.debug("Track polyphony is now: '" + self.polyphony.getWording() + "' (was '" + old_polyphony + "'.)")

if __name__ == "__main__":
    logger.debug("Execution of module 'track'")
    gg = Genre("genre")
    aa = Artist("artist")
    aall = Album("album")
    ff = Format("format")
    pp = Polyphony("polyphony")
    t = Track(title="title",duration=111,path="src/chemin/",genre=gg,artist=aa,album=aall,format=ff,polyphony=pp)
    logger.debug("Title: " + t.getTitle())
    logger.debug("Artist: " + t.getArtist().getStagename())
    art2 = Artist("Artist 2")
    t.setArtist(art2)
    logger.debug("Artist: " + t.getArtist().getStagename())
    logger.debug("End of the execution of the module 'track'")
else:
    logger.debug("Loading of module 'track'")
