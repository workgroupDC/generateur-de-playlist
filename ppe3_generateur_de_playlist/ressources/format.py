# -*- coding: utf-8 -*-
"""
Created on Tue Oct  4 13:39:45 2016

@author: Clément CAUDAL
"""

import logging
logger = logging.getLogger("format")
logger.setLevel(logging.DEBUG)

from logging.handlers import RotatingFileHandler

""" Write in a syslog 'dclog.log'
"""
formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(filename)s:%(message)s')
file_handler = RotatingFileHandler('dclog.log','a',1000000,1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)



class Format(object):
	def __init__(self, wording):
         """Constructor of class Format
         """
         logger.debug("Instanciation of an 'format'")
         self.wording = str(wording)


	#----GETTERS----#
	def getWording(self):
         """Return wording format as a str()
         """
         return self.wording



	#----SETTERS----#
	def setWording(self, _wording):
         """Set format wording using the given parameter str()
         """
         old_wording = self.wording # new variable old_wording which takes the value of the former wording of the format
         self.wording = str(_wording) # modification of the format wording
         logger.debug("Format wording is now '" + self.wording + "' (was '" + old_wording + "')") # message of modification format wording




if __name__ == "__main__":
    logger.debug("Execution of module 'format.py'")
    ff = Format("Test")
    print(ff.getWording())
    logger.debug("At the end of execution of the module 'format.py")
else:
    logger.debug("Loading of module 'format.py'")

