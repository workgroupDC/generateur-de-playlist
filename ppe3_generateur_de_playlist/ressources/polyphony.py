# -*- coding: utf-8 -*-
"""
Created on Mon Oct  3 22:39:31 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("polyphony")
logger.setLevel(logging.DEBUG)

from logging.handlers import RotatingFileHandler

""" Write in a syslog 'dclog.log'
"""
formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(filename)s:%(message)s')
file_handler = RotatingFileHandler('dclog.log','a',1000000,1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)



class Polyphony(object):
    def __init__(self, wording):
        """Polyphony constructor
        """
        logger.debug("Instantiation of a 'Polyphony'")
        self.wording = str(wording)
        
    def getWording(self):
        """Return polyphony wording as a str()
        """
        return self.wording
     
    def setWording(self, _wording):
        """Set polyphony wording using the given parameter str()
        """
        old_wording = self.wording
        self.wording = str(_wording)
        logger.debug("Polyphony wording is now: '" + self.wording + "' (was '" + old_wording + "'.)")
 
if __name__ == "__main__":
    logger.debug("Execution of module 'polyphony'")
    pp = Polyphony("Test")
    print(pp.getWording())
    logger.debug("End of the execution of the module 'polyphony'")
else:
    logger.debug("Loading of module 'polyphony'")
