# -*- coding: utf-8 -*-
"""
Created on Mon Oct  3 21:46:40 2016

@author: etudiant
"""

import logging
logger = logging.getLogger("album")
logger.setLevel(logging.DEBUG)

from logging.handlers import RotatingFileHandler

""" Write in a syslog 'dclog.log'
"""
formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(filename)s:%(message)s')
file_handler = RotatingFileHandler('dclog.log','a',1000000,1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)



class Album(object):
    def __init__(self, title):
        """Album constructor
        """
        logger.debug("Instantiation of a 'Album'")
        self.title = str(title)
                 
    def getTitle(self):
        """Return album title name as a str()
        """
        return self.title
     
    def setTitle(self, _title):
        """Set album title using the given parameter str()
        """
        old_title = self.title
        self.title = str(_title)
        logger.debug("Album title is now: '" + self.title + "' (was '" + old_title + "'.)")
 
if __name__ == "__main__":
    logger.debug("Execution of module 'album'")
    a = Album("test")
    print(a.getTitle())
    logger.debug("End of the execution of the module 'album'")
else:
    logger.debug("Loading of module 'album'")
