# -*- coding: utf-8 -*-
"""
Created on Mon Oct  3 22:51:09 2016

@author: etudiant
"""

import logging
from logging.handlers import RotatingFileHandler
logger = logging.getLogger("genre")
logger.setLevel(logging.DEBUG)

""" Write in a syslog 'dclog.log'
"""
formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(filename)s:%(message)s')
file_handler = RotatingFileHandler('dclog.log','a',1000000,1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

class Genre(object):
    def __init__(self, wording):
        """Genre constructor
        """
        self.wording = str(wording)
        logger.debug("Instantiation of a 'Genre' with 'wording' as '%s'" % (self.wording))
                 
    def getWording(self):
        """Return genre wording as a str()
        """
        return self.wording
     
    def setWording(self, _wording):
        """Set genre wording using the given parameter str()
        """
        old_wording = self.wording
        self.wording = str(_wording)
        logger.debug("Genre wording is now: '" + self.wording + "' (was '" + old_wording + "'.)")

if __name__ == "__main__":
    logger.debug("Execution of module 'genre'")
    gg = Genre("Test")
    print(gg.getWording())
    gg.setWording("new wording")
    print(gg.getWording())
    logger.debug("End of the execution of the module 'genre'")
else:
    logger.debug("Loading of module 'genre'")
