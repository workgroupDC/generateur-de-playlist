# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 17:01:52 2016

@author: Donovan
"""

import logging
from logging.handlers import RotatingFileHandler
logger = logging.getLogger("artist")
logger.setLevel(logging.DEBUG)

""" Write in a syslog 'dclog.log'
"""
formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(filename)s:%(message)s')
file_handler = RotatingFileHandler('dclog.log','a',1000000,1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


class Artist(object):
    def __init__(self, stagename):
        """Artist constructor
        """
        logger.debug("Instantiation of a 'Artist'")
        self.stagename = str(stagename)
        
    def getStagename(self):
        """Return artist stage name as a str()
        """
        return self.stagename
    
    def setStagename(self, _stagename):
        """Set artist stage name using the given parameter str()
        """
        old_stagename = self.stagename
        self.stagename = str(_stagename)
        logger.debug("Artist stage name is now: '" + self.stagename + "' (was '" + old_stagename + "'.)")
 
if __name__ == "__main__":
    logger.debug("Execution of module 'artist'")
    aa = Artist("Test")
    print(aa.getStagename())
    logger.debug("End of the execution of the module 'artist'")
else:
    logger.debug("Loading of module 'artist'")
