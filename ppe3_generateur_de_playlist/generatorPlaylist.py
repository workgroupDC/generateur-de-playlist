# -*- coding: utf-8 -*-
"""
Created on Tue Nov 15 15:07:10 2016

@author: etudiant
"""

import logging
from logging.handlers import RotatingFileHandler
from .ressources.genre import Genre
from .ressources.artist import Artist
from .ressources.album import Album
from .ressources.track import Track
from .ressources.format import Format
from .ressources.polyphony import Polyphony
from .ressources.playlist import Playlist
import postgresql
import argparse

def main():
    db = postgresql.open('pq://clement_donovan_user:dcplaylist@172.16.99.2:5432/radio_libre')
    ps = db.prepare("SELECT * FROM clement_donovan.track INNER JOIN clement_donovan.genre ON track.id_genre = genre.id_genre INNER JOIN clement_donovan.artist ON track.id_artist = artist.id_artist INNER JOIN clement_donovan.album ON album.id_album = track.id_album INNER JOIN clement_donovan.format ON format.id_format = track.id_format INNER JOIN clement_donovan.polyphony ON polyphony.id_polyphony = track.id_polyphony")
    var = ps()
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-n", "--name",help="Add a name to playlist", dest="name", nargs = 1, action='append', required=True)
    parser.add_argument("-d", "--duration", dest="duration", type=int, default = 10000, help="Add a duration to playlist (default is 10 000s)")
    parser.add_argument("-g", "--genre",metavar=('genre','percentage'),help="Percentage associated to a genre in playlist - Ex : Rock ou Electronic...", dest="genre", nargs = 2, action='append')
    parser.add_argument("-art", "--artist", dest="artist", nargs = 2, action='append', metavar=('artist','percentage'),help="Percentage associated to a artist in playlist - Ex : Absentia ou Autechre...")
    parser.add_argument("-alb", "--album",metavar=('album','percentage'),help="Percentage associated to a album in playlist - Ex : reversion ou 666 ou Permutation ...", dest="album", nargs = 2, action='append')
    parser.add_argument("--xspf", dest="xspf", action="store_true",help="Export playlist into a xspf file (if not informed then it's export in m3u)")
    args = parser.parse_args()

    listGenre = []
    listArtist = []
    listAlbum = []
    listTrack = []
    newAlb = Album("aaa")
    newArt = Artist("aaa")
    newGen = Genre("aaa")
    newForm = Format("aaa")
    newPol = Polyphony("aaa")
    listAlbum.append(newAlb)
    listArtist.append(newArt)
    listGenre.append(newGen)
    a = 0
    p = 0
    tg = 0
    ta = 0
    talb = 0
    genre = []
    artist = []
    album = []
    
    if args.genre:
        for g in args.genre:
            p = int(p) + int(g[1])  
            tg = args.duration * int(g[1]) / 100
            aa = [g[0],g[1],tg]
            genre.append(aa)
            
        
    if args.artist:
        for art in args.artist:
            p = int(p) + int(art[1])
            ta = args.duration * int(art[1]) / 100
            aaa = [art[0],art[1],ta]
            artist.append(aaa)
            
    if args.album:
        for alb in args.album:
            p = int(p) + int(alb[1])
            talb = args.duration * int(alb[1]) / 100
            aaaa = [alb[0],alb[1],talb]
            album.append(aaaa)
        
    if int(p) < 101:
        if int(p) < 100:
            tempsRest = 100 - int(p)
            tempsRest2 = args.duration * int(tempsRest) / 100
            for t in var:
                if tempsRest2 > t[3]:
                    newT3 = Track(t[1],t[3],t[2],newAlb,newArt,newGen,newForm,newPol)
                    listTrack.append(newT3)
                    a = a + t[3]
                    tempsRest2 = int(tempsRest2) - int(t[3])
        
        if args.genre:
            for g in genre:
                for t in var:
                    if t[10] == g[0] and int(g[2]) > t[3]:
                        newT = Track(t[1],t[3],t[2],newAlb,newArt,newGen,newForm,newPol)
                        listTrack.append(newT)
                        a = a + t[3]
                        g[2] = int(g[2]) - int(t[3])
                    
        if args.artist:
            for art in artist:
                for t in var:
                    if t[12] == art[0] and int(art[2]) > t[3]:
                        newT2 = Track(t[1],t[3],t[2],newAlb,newArt,newGen,newForm,newPol)
                        listTrack.append(newT2)
                        a = a + t[3]
                        art[2] = int(art[2]) - int(t[3])
        
        if args.album:
            for alb in album:
                for t in var:
                    if t[14] == alb[0] and int(alb[2] > t[3]):
                        newT3 = Track(t[1],t[3],t[2],newAlb,newArt,newGen,newForm,newPol)
                        listTrack.append(newT3)
                        a = a + t[3]
                        alb[2] = int(alb[2]) - int(t[3])
                        
        
        tempsB = 0
        if not args.artist and not args.genre and not args.album:
            for t in var:
                if tempsB > t[3]:
                    newT4 = Track(t[1],t[3],t[2],newAlb,newArt,newGen,newForm,newPol)
                    listTrack.append(newT4)
                    a = a + t[3]
                    tempsB = int(tempsB) - int(t[3])
        
        print("La playlist fait : "+str(a)+" secondes.")
        
        p = Playlist(name=args.name[0][0],
                 duration=a,
                 listgenre=listGenre,
                 listartist=listArtist,
                 listalbum=listAlbum,
                 listtrack=listTrack)
    
        if args.xspf:
            p.exportPlaylist("xspf")
        else:
            p.exportPlaylist("m3u")
    else:
        print("Erreur de pourcentage")
	
    

    
    
if __name__ == "__main__":
    main()
    
