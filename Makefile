all: doc source deb

.PHONY: doc
doc:
	$(MAKE) -C doc

deb: setup.py stdeb.cfg
	python3 setup.py --command-packages=stdeb.command bdist_deb

source: setup.py
	python3 setup.py sdist
